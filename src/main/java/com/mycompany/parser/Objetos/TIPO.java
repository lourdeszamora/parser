/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.parser.Objetos;

/**
 *
 * @author Lourdes Zamora
 */
public enum TIPO {
    ID, ADD_OP, MULT_OP, LOG_OP, REL_OP, ASIG, PERIOD, DDPERIOD, FALSE_LITTERAL,
    TRUE_LITTERAL, INT_LITTERAL, REAL_LITTERAL, STRING_LITTERAL, DATE_LITTERAL, WHILE, IF, ELSE,
    RETURN, STATIC_TKN, INT_TYPE, ANY_TYPE, BOOLEAN_TYPE, VOID_TYPE, REAL_TYPE, STRING_TYPE, DATE_TYPE,
    ANYTYPE_TYPE,
    INDEFINIDO
}
