/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.parser.Objetos;

/**
 *
 * @author Lourdes Zamora
 */
public class Token {
    public String valor;
    public TIPO tipo;
    
    public Token(String valor)
    {
        this.valor= valor;
        this.tipo= Identificar(valor);
    }
    
    private TIPO Identificar(String valor){
        if(valor.matches("([a-zA-Z]|_){1}([a-zA-Z]|[0-9])*"))
            return TIPO.ID;
        if(valor.matches("(\\+|-){1}"))
            return TIPO.ADD_OP;
        if(valor.matches("(\\*|\\/){1}"))
            return TIPO.MULT_OP;
        if(valor.matches("&&|\\|\\|"))
            return TIPO.LOG_OP;
        if(valor.matches("<=|>=|>|<|=="))
            return TIPO.REL_OP;
        if(valor.matches(":="))
            return TIPO.ASIG;
        if(valor.matches("."))//probar
            return TIPO.PERIOD;
        if(valor.matches("::"))
            return TIPO.DDPERIOD;
        if(valor.matches("false"))
            return TIPO.FALSE_LITTERAL;
        if(valor.matches("true"))
            return TIPO.TRUE_LITTERAL;
        if(valor.matches("[0-9]+"))
            return TIPO.INT_LITTERAL;
        if(valor.matches("[0-9]+.[0-9]+"))//PROBAR
            return TIPO.REAL_LITTERAL;
        if(valor.matches("\"([a-zA-Z]|[0-9]| |%|@|,|-|=|\\(|\\)|_)*\""))
            return TIPO.STRING_LITTERAL;
        if(valor.matches(" [0-9][0-9]/[0-9][0-9]/[0-9][0-9][0-9][0-9]"))
            return TIPO.DATE_LITTERAL;
        if(valor.matches("while"))
            return TIPO.WHILE;
        if(valor.matches("if"))
            return TIPO.IF;
        if(valor.matches("else"))
            return TIPO.ELSE;
        if(valor.matches("exit"))
            return TIPO.RETURN;
        if(valor.matches("static"))
            return TIPO.STATIC_TKN;
        if(valor.matches("int"))
            return TIPO.INT_TYPE;
        if(valor.matches("anytype"))
            return TIPO.ANYTYPE_TYPE;
        if(valor.matches("boolean"))
            return TIPO.BOOLEAN_TYPE;
        if(valor.matches("void"))
            return TIPO.VOID_TYPE;
        if(valor.matches("real"))
            return TIPO.REAL_TYPE;
        if(valor.matches("string"))
            return TIPO.STRING_TYPE;
        if(valor.matches("date"))
            return TIPO.DATE_TYPE;
        return TIPO.INDEFINIDO;
    }
    
}
