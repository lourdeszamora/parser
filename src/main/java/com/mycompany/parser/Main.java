/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.parser;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Lourdes Zamora
 */
public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String ubicacion = "";
        System.out.println("Ingrese la dirección del achivo a parsear");
        ubicacion = in.nextLine();
        System.out.println("Leyendo el archivo...");
        File archivo = new File(ubicacion);
        try {
            FileInputStream fis = new FileInputStream(archivo);
            byte[] data = new byte[(int) archivo.length()];
            fis.read(data);
            fis.close();
            String str = new String(data, "UTF-8");
            System.out.println("Archivo leido con exito");
            System.out.println("Ejecutando parser");
            
            Parser p = new Parser(str);
            p.Ejecutar();
            
            
        } catch (FileNotFoundException ex) {
            System.out.println("Error! el archivo no existe.");
        } catch (IOException ex) {
            System.out.println("Error! no se pudo leer el archivo.");
        }

    }
}
