/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.parser;

import com.mycompany.parser.Objetos.Token;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author Lourdes Zamora
 */
public class Tokenizer {

    ArrayList<Token> tokens = new ArrayList<>();
    ArrayList<Token> tokensSafePoint = new ArrayList<>();
    public Tokenizer(String cadena) {
        StringTokenizer st = new StringTokenizer(cadena);
        while (st.hasMoreTokens()) {
            tokens.add(new Token(st.nextToken()));
        }
    }
    public Token GetNextToken() {
        Token token= tokens.get(0);
        tokens.remove(0);
        return token;
    }
    
    public boolean HasValues(){
        return !tokens.isEmpty();
    }
    
    public void CreateSafePoint(){
        tokensSafePoint = new ArrayList(tokens.subList(0, tokens.size()-1));
    }
    
    public void CargarSafePoint(){
        tokens= new ArrayList(tokensSafePoint.subList(0,tokens.size()-1));
    }
}