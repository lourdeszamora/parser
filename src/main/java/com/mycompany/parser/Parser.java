/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.parser;

import com.mycompany.parser.Objetos.TIPO;
import com.mycompany.parser.Objetos.Token;

/**
 *
 * @author Lourdes Zamora
 */
public class Parser {

    private Tokenizer tokenizer;

    public Parser(String contenido) {
        System.out.println("Iniciando Tokenizer");
        tokenizer = new Tokenizer(contenido);
    }

    public void Ejecutar() {
        System.out.println("Ejecutando parser");
        System.out.println(EvaluateProg());

    }

    public String EvaluateProg() {
        //  System.out.println("prog => optional_func_dec");
        if (tokenizer.HasValues()) {
            String producciones = "prog => optional_func_dec\n";
            return optional_func_dec(producciones);
        } else {
            return "Error";
        }

    }

    private String optional_func_dec(String producciones) {
        //string 

        String func_head = func_head();
        String body = body();
        String optional_func_dec;

        if (tokenizer.HasValues()) {
            producciones += "optional_func_dec => func_head body optional_func_dec\n";
            optional_func_dec = optional_func_dec(producciones);
            producciones += func_head;
            producciones += body;
            producciones += optional_func_dec;
            return producciones;
        } else {
            producciones += "optional_func_dec => func_head body\n";
            producciones += func_head;
            producciones += body;
            return producciones;
        }
    }

    private String func_head() {
        String producciones = "func_head => func_name param_list_opt\n";
        String func_name = func_name();
        String param_list_opt = param_list_opt();

        return producciones += func_name + param_list_opt;
    }

    private String body() {
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.INDEFINIDO && t.valor.matches("\\{")) {
                String stmt_list = stmt_list();
                if (tokenizer.HasValues()) {
                    t = tokenizer.GetNextToken();
                    if (t.tipo == TIPO.INDEFINIDO && t.valor.matches("\\}")) {
                        String prod = "body => { stmt_list }\n";
                        return prod + stmt_list;
                    } else {
                        return "body => ERROR el token encontrado no es de tipo }\n";
                    }
                } else {
                    return "body => ERROR la cadena se quedo sin tokens\n";
                }
            } else {
                return "body => ERROR el token encontrado no es de tipo {\n";
            }
        } else {
            return "body => ERROR la cadena se quedo sin tokens\n";
        }
    }

    private String func_name() {
        String func_type = func_type();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.ID) {
                String prod = "func_name => func_type ID(" + t.valor + ")\n";
                return prod + func_type;
            } else {
                return "func_name => ERROR el token encontrado no es de tipo ID\n";
            }
        }
        return "func_name => ERROR la cadena se quedo sin tokens\n";
    }

    private String param_list_opt() {
        tokenizer.CargarSafePoint();
        String param_list = param_list();
        if (param_list.contains("ERROR")) {
            return "param_list => ε";
        }
        return "param_list_opt => param_list\n" + param_list;
    }

    private String func_type() {
        String func_modifiers = func_modifiers();
        String decl_type = decl_type();
        String prod = "func_type => func_modifiers decl_type\n";
        return prod + func_modifiers + decl_type;
    }

    private String func_modifiers() {
        tokenizer.CreateSafePoint();
        String func_mods = func_mods();
        if (func_mods.contains("ERROR")) {
            tokenizer.CargarSafePoint();
            String prod = "func_modifiers => ε\n";
            return prod;
        } else {
            String prod = "func_modifiers => func_mods\n";
            return prod + func_mods;
        }
    }

    private String decl_type() {
        String prod = "decl_type => ";
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (null == t.tipo) {
                return prod + "ERROR el token no es del tipo esperado\n";
            } else {
                switch (t.tipo) {
                    case ANYTYPE_TYPE:
                        return prod + "ANYTYPE_TYPE(" + t.valor + ")\n";
                    case BOOLEAN_TYPE:
                        return prod + "BOOLEAN_TYPE(" + t.valor + ")\n";
                    case VOID_TYPE:
                        return prod + "VOID_TYPE(" + t.valor + ")\n";
                    case REAL_TYPE:
                        return prod + "REAL_TYPE(" + t.valor + ")\n";
                    case STRING_TYPE:
                        return prod + "STRING_TYPE(" + t.valor + ")\n";
                    case DATE_TYPE:
                        return prod + "DATE_TYPE(" + t.valor + ")\n";
                    default:
                        return prod + "ERROR el token no es del tipo esperado\n";
                }
            }
        } else {
            return prod + "ERROR ya no hay mas tokens\n";
        }
    }

    private String func_mods() {
        String prod = "func_mods =>STATIC_TKN(";
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.STATIC_TKN) {
                return prod + t.valor + ")\n";
            } else {
                return "func_mods => ERROR el token no es del tipo esperado\n";
            }
        } else {
            return "func_mods => ERROR ya no hay tokens en la cadena\n";
        }
    }

    private String param_list() {
        tokenizer.CreateSafePoint();
        String decl_param = decl_param();
        if (tokenizer.HasValues()) {
            Token nextT = tokenizer.GetNextToken();
            if (nextT.tipo == TIPO.INDEFINIDO && nextT.valor.matches(",")) {
                tokenizer.CargarSafePoint();
                String param_list = param_list();
                if (tokenizer.HasValues()) {
                    Token t = tokenizer.GetNextToken();
                    if (t.tipo == TIPO.INDEFINIDO && t.valor.matches(",")) {
                        decl_param = decl_param();
                        return "param_list => param_list , decl_param\n" + param_list + decl_param;
                    } else {
                        return "param_list => ERROR no es el token esperado\n";
                    }
                } else {
                    return "param_list => ERROR ya no hay tokens\n";
                }
            } else {
                tokenizer.CargarSafePoint();
                decl_param = decl_param();
                return "param_list => ERROR decl_param\n" + decl_param;
            }
        } else {
            return "param_list => ERROR ya no hay tokens\n";
        }
    }

    private String decl_param() {
        String decl_type = decl_type();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.ID) {
                String prod = "decl_param => decl_type ID(" + t.valor + ")\n";
                return prod + decl_type;
            } else {
                return "decl_param => ERROR el token encontrado no es de tipo ID\n";
            }
        }
        return "decl_param => ERROR la cadena se quedo sin tokens\n";
    }

    private String stmt_list() {
        tokenizer.CreateSafePoint();
        String stmts = stmts();
        if (stmts.contains("ERROR")) {
            tokenizer.CargarSafePoint();
            String prod = "stmt_list => ε\n";
            return prod;
        } else {
            String prod = "stmt_list => stmts\n";
            return prod + stmts;
        }
    }

    private String stmts() {
        tokenizer.CreateSafePoint();
        String stmt = stmt();
        String stmt2 = stmt();

        if (stmt2.contains("ERROR")) {
            tokenizer.CargarSafePoint();
            stmt = stmt();
            return "stmts => stmt\n" + stmt;
        } else {
            tokenizer.CargarSafePoint();
            String stmts = stmts();
            stmt = stmt();
            return "stmts => stmts stmt\n" + stmts + stmt;
        }
    }

    private String stmt() {
        tokenizer.CreateSafePoint();
        String compound_stmt = compound_stmt();
        String prod = "stmt => ";
        if (!compound_stmt.contains("ERROR")) {
            return prod + "compound_stmt\n" + compound_stmt;
        } else {
            tokenizer.CargarSafePoint();
            String while_stmt = while_stmt();
            if (!while_stmt.contains("ERROR")) {
                return prod + "while_stmt\n" + while_stmt;
            } else {
                tokenizer.CargarSafePoint();
                String if_stmt = if_stmt();
                if (!if_stmt.contains("ERROR")) {
                    return prod + "if_stmt\n" + if_stmt;
                } else {
                    tokenizer.CargarSafePoint();
                    String expr_stmt = expr_stmt();
                    if (!expr_stmt.contains("ERROR")) {
                        return prod + "expr_stmt\n" + expr_stmt;
                    } else {
                        tokenizer.CargarSafePoint();
                        String return_stmt = return_stmt();
                        if (!return_stmt.contains("ERROR")) {
                            return prod + "return_stmt\n" + return_stmt;
                        }
                    }
                }
            }
        }
        return prod + "ERROR\n";
    }

    private String compound_stmt() {
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.INDEFINIDO && t.valor.matches("\\{")) {
                String stmt_list = stmt_list();
                if (tokenizer.HasValues()) {
                    Token t2 = tokenizer.GetNextToken();
                    if (t2.tipo == TIPO.INDEFINIDO && t.valor.matches("\\}")) {
                        return "compound_stmt => { stmt_list }\n" + stmt_list;
                    } else {
                        return "compound_stmt => ERROR no es el token esperado\n";
                    }
                } else {
                    return "compound_stmt => ERROR ya no hay tokens\n";
                }
            } else {
                return "compound_stmt => ERROR no es el token esperado\n";
            }
        } else {
            return "compound_stmt => ERROR ya no hay tokens\n";
        }
    }

    private String while_stmt() {
        String while_test = while_test();
        String stmt = stmt();
        return "while_stmt => while_test stmt\n" + while_test + stmt;
    }

    private String if_stmt() {
        tokenizer.CreateSafePoint();
        String if_conds = if_conds();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.ELSE) {
                tokenizer.CargarSafePoint();
                String else_stmt = else_stmt();
                return "if_stmt => else_stmt\n" + else_stmt;
            } else {
                return "if_stmt => if_conds\n" + if_conds;
            }
        } else {
            return "if_stmt => if_conds\n" + if_conds;
        }
    }

    private String expr_stmt() {
        tokenizer.CreateSafePoint();
        String prod = "expr_stmt => ";
        String function;
        String eval;
        String asg_stmt = asg_stmt();
        if (!asg_stmt.contains("ERROR")) {
            prod = prod + "asg_stmt ;\n" + asg_stmt;
        } else {
            tokenizer.CargarSafePoint();
            function = function();
            prod = prod + "function\n" + function;
            if (!function.contains("ERROR")) {
                prod = prod + "function ;\n" + function;
            } else {
                tokenizer.CargarSafePoint();
                eval = eval();
                prod = prod + "eval ;\n" + eval;
            }
        }
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.INDEFINIDO && t.valor.matches(";")) {
                return prod;
            } else {
                return "expr_stmt => ERROR se esperaba ;";
            }
        } else {
            return "expr_stmt => ERROR ya no hay tokens";
        }
    }

    private String return_stmt() {
        tokenizer.CreateSafePoint();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.RETURN) {
                String bool_expr = bool_expr();
                if (bool_expr.contains("Error")) {
                    tokenizer.CargarSafePoint();
                    if (tokenizer.HasValues()) {
                        t = tokenizer.GetNextToken();
                        if (t.tipo == TIPO.RETURN) {
                            if (tokenizer.HasValues()) {
                                t = tokenizer.GetNextToken();
                                if (t.valor.matches(";")) {
                                    return "return_stmt => RETURN ;\n";
                                } else {
                                    return "return_stmt => ERROR se esperaba ;\n";
                                }
                            } else {
                                return "return_stmt => ERROR ya no hay tokens\n";
                            }
                        } else {
                            return "return_stmt => ERROR se esperaba return\n";
                        }
                    } else {
                        return "return_stmt => ERROR ya no hay tokens\n";
                    }
                } else {
                    t = tokenizer.GetNextToken();
                    if (t.valor.matches(";")) {
                        return "return_stmt => RETURN bool_expr ;\n" + bool_expr;
                    } else {
                        return "return_stmt => ERROR se esperaba ;\n";
                    }
                }
            } else {
                return "return_stmt => ERROR se esperaba return\n";
            }
        } else {
            return "return_stmt => ERROR ya no hay tokens\n";
        }
    }

    private String while_test() {
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.WHILE) {
                if (tokenizer.HasValues()) {
                    Token t2 = tokenizer.GetNextToken();
                    if (t.tipo == TIPO.INDEFINIDO && t.valor.matches("\\(")) {
                        String bool_expr = bool_expr();
                        if (tokenizer.HasValues()) {
                            Token t3 = tokenizer.GetNextToken();
                            if (t.tipo == TIPO.INDEFINIDO && t.valor.matches("\\)")) {
                                return "while_test =>  while ( bool_expr )\n" + bool_expr;
                            } else {
                                return "while_test => ERROR se esperaba )\n";
                            }
                        } else {
                            return "while_test => ERROR ya no hay mas tokens\n";
                        }
                    } else {
                        return "while_test => ERROR se esperaba (\n";
                    }
                } else {
                    return "while_test => ERROR ya no hay mas tokens\n";
                }
            } else {
                return "while_test => ERROR se esperaba while\n";
            }
        } else {
            return "while_test => ERROR ya no hay mas tokens\n";
        }
    }

    private String bool_expr() {
        tokenizer.CreateSafePoint();
        String expr = expr();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.LOG_OP) {
                tokenizer.CargarSafePoint();
                String bool_expr1 = bool_expr1();
                return bool_expr1;
            } else {
                return "bool_expr => expr\n" + expr;
            }
        } else {
            return "bool_expr => expr\n" + expr;
        }
    }

    private String if_cond() {
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.IF) {
                if (tokenizer.HasValues()) {
                    t = tokenizer.GetNextToken();
                    if (t.tipo == TIPO.INDEFINIDO && t.valor.matches("\\(")) {
                        String bool_expr = bool_expr();
                        if (tokenizer.HasValues()) {
                            t = tokenizer.GetNextToken();
                            if (t.tipo == TIPO.INDEFINIDO && t.valor.matches("\\)")) {
                                return "if_cond => ( bool_expr )\n" + bool_expr;
                            } else {
                                return "if_cond => ERROR el token no es )\n";
                            }
                        } else {
                            return "if_cond => ERROR ya no hay mas tokens\n";
                        }
                    } else {
                        return "if_cond => ERROR el token no es (\n";
                    }
                } else {
                    return "if_cond => ERROR ya no hay mas tokens\n";
                }
            } else {
                return "if_cond => ERROR el token no es if\n";
            }
        } else {
            return "if_cond => ERROR ya no hay mas tokens\n";
        }
    }

    private String if_conds() {
        String if_cond = if_cond();
        String stmt = stmt();
        return "if_conds => if_cond stmt\n" + if_cond + stmt;
    }

    private String else_stmt() {
        String else_r = else_r();
        String stmt = stmt();
        return "else_stmt => else_r stmt\n" + else_r + stmt;
    }

    private String else_r() {
        String if_conds = if_conds();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.ELSE) {
                return "else_r => if_conds ELSE\n" + if_conds;
            } else {
                return "else_r => ERROR el token no es else\n";
            }
        } else {
            return "else_r => ERROR no hay mas tokens\n";
        }
    }

    private String asg_stmt() {
        tokenizer.CreateSafePoint();
        String asg_stmt1 = asg_stmt1();
        if (!asg_stmt1.contains("ERROR")) {
            return asg_stmt1;
        } else {
            tokenizer.CargarSafePoint();
            String asg_stmt2 = asg_stmt2();
            return asg_stmt2;
        }
    }

    private String function() {
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.ID) {
                if (tokenizer.HasValues()) {
                    t = tokenizer.GetNextToken();
                    if (t.valor.matches("\\(")) {
                        String par_list = par_list();
                        if (tokenizer.HasValues()) {
                            t = tokenizer.GetNextToken();
                            if (t.valor.matches("\\)")) {
                                return "function => ( par_list )\n" + par_list;
                            } else {
                                return "function => ERROR esperaba )";
                            }
                        } else {
                            return "function => ERROR ya no hay tokens";
                        }
                    } else {
                        return "function => ERROR esperaba (";
                    }
                } else {
                    return "function => ERROR ya no hay tokens";
                }
            } else {
                return "function => ERROR esperaba ID";
            }
        } else {
            return "function => ERROR ya no hay tokens";
        }
    }

    private String eval() {
        String eval_name = eval_name();
        String par_list = par_list();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.INDEFINIDO && t.valor.matches("\\)")) {
                return "eval => eval_name par_list )\n" + eval_name + par_list;
            } else {
                return "eval => ERROR se esperaba )\n";
            }
        } else {
            return "eval => ERROR ya no hay tokens";
        }
    }

    private String asg_stmt1() {
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.ID) {
                t = tokenizer.GetNextToken();
                if (t.tipo == TIPO.ASIG) {
                    String bool_expr = bool_expr();
                    return "asg_stmt => ID ASIG bool_expr\n" + bool_expr;
                } else {
                    return "asg_stmt => ERROR se esperaba un token de tipo ASIG\n";
                }
            } else {
                return "asg_stmt => ERROR se esperaba un token de tipo ID\n";
            }
        } else {
            return "asg_stmt => ERROR ya no hay tokens\n";
        }
    }

    private String asg_stmt2() {
        String field = field();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.ASIG) {
                String bool_expr = bool_expr();
                return "asg_stmt => ID ASIG bool_expr\n" + bool_expr;
            } else {
                return "asg_stmt => ERROR se esperaba un token de tipo ASIG\n";
            }
        } else {
            return "asg_stmt => ERROR ya no hay tokens\n";
        }
    }

    private String field() {
        String qualifier = qualifier();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.ID) {
                return "field => qualifier ID\n" + qualifier;
            } else {
                return "field => ERROR se esperaba un token de tipo ID\n";
            }
        } else {
            return "field => ERROR ya no hay tokens\n";
        }
    }

    private String qualifier() {
        tokenizer.CreateSafePoint();
        String qualifier1 = qualifier1();
        if (!qualifier1.contains("ERROR")) {
            return qualifier1;
        } else {
            tokenizer.CargarSafePoint();
            String qualifier2 = qualifier2();
            if (!qualifier2.contains("ERROR")) {
                return qualifier2;
            } else {
                tokenizer.CargarSafePoint();
                String qualifier3 = qualifier3();
                return qualifier3;
            }
        }
    }

    private String qualifier1() {
        String eval = eval();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.PERIOD) {
                return "qualifier => eval PERIOD\n" + eval;
            } else {
                return "qualifier => ERROR se esperaba un .\n";
            }
        } else {
            return "qualifier => ERROR ya no hay tokens.\n";
        }
    }

    private String qualifier2() {
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.ID) {
                if (tokenizer.HasValues()) {
                    t = tokenizer.GetNextToken();
                    if (t.tipo == TIPO.PERIOD) {
                        return "qualifier => ID PERIOD\n";
                    } else {
                        return "qualifier => ERROR se esperaba un .\n";
                    }
                } else {
                    return "qualifier => ERROR ya no hay tokens.\n";
                }
            } else {
                return "qualifier => ERROR se esperaba un ID\n";
            }
        } else {
            return "qualifier => ERROR ya no hay tokens.\n";
        }
    }

    private String qualifier3() {
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.ID) {
                if (tokenizer.HasValues()) {
                    t = tokenizer.GetNextToken();
                    if (t.tipo == TIPO.DDPERIOD) {
                        return "qualifier => ID DDPERIOD\n";
                    } else {
                        return "qualifier => ERROR se esperaba un ::\n";
                    }
                } else {
                    return "qualifier => ERROR ya no hay tokens.\n";
                }
            } else {
                return "qualifier => ERROR se esperaba un ID\n";
            }
        } else {
            return "qualifier => ERROR ya no hay tokens.\n";
        }
    }

    private String eval_name() {
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.ID) {
                if (tokenizer.HasValues()) {
                    t = tokenizer.GetNextToken();
                    if (t.tipo == TIPO.INDEFINIDO && t.valor.matches("\\(")) {
                        return "qualifier => ID )\n";
                    } else {
                        return "qualifier => ERROR se esperaba un )\n";
                    }
                } else {
                    return "qualifier => ERROR ya no hay tokens.\n";
                }
            } else {
                return "qualifier => ERROR se esperaba un ID\n";
            }
        } else {
            return "qualifier => ERROR ya no hay tokens.\n";
        }
    }

    private String par_list() {
        tokenizer.CreateSafePoint();
        String prm_list = prm_list();
        if (prm_list.contains("ERROR")) {
            tokenizer.CargarSafePoint();
            return "par_list => ε\n";
        } else {
            return "par_list => prm_list\n" + prm_list;
        }
    }

    private String prm_list() {
        tokenizer.CreateSafePoint();
        String bool_expr = bool_expr();
        String prod = "prm_list => ";
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.INDEFINIDO && t.valor.matches(",")) {
                tokenizer.CargarSafePoint();
                String prm_list = prm_list();
                if (tokenizer.HasValues()) {
                    t = tokenizer.GetNextToken();
                    if (t.tipo == TIPO.INDEFINIDO && t.valor.matches(",")) {
                        bool_expr = bool_expr();
                        return "prm_list => prm_list , bool_expr\n" + prm_list + bool_expr;
                    } else {
                        return "prm_list => ERROR se esperaba ,\n";
                    }
                } else {
                    return "prm_list => ERROR no hay mas tokens\n";
                }
            } else {
                return "prm_list => bool_expr\n" + bool_expr;
            }
        } else {
            return "prm_list => bool_expr\n" + bool_expr;
        }
    }

    private String expr() {
        String simple_expr = simple_expr();
        tokenizer.CreateSafePoint();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.REL_OP) {
                String simple_expr2 = simple_expr();
                return "expr => simple_expr REL_OP(" + t.valor + ") simple_expr\n" + simple_expr + simple_expr2;
            } else {
                tokenizer.CargarSafePoint();
                return "expr => simple_expr\n" + simple_expr;
            }
        } else {
            //tokenizer.CargarSafePoint();
            return "expr => simple_expr\n" + simple_expr;
        }
    }

    private String bool_expr1() {
        String bool_expr = bool_expr();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.LOG_OP) {
                String expr = expr();
                return "bool_expr => bool_expr LOG_OP(" + t.valor + ") expr\n" + bool_expr + expr;
            } else {
                return "bool_expr => ERROR se esperaba un LOG_OP\n";
            }
        } else {
            return "bool_expr => ERROR no hay mas tokens\n";
        }
    }

    private String simple_expr() {
        tokenizer.CreateSafePoint();
        String term = term();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.ADD_OP) {
                tokenizer.CargarSafePoint();
                String simple_expr = simple_expr1();
                return simple_expr;
            } else {
                return "simple_expr => term\n" + term;
            }
        } else {
            return "simple_expr => term\n" + term;
        }
    }

    private String term() {
        tokenizer.CreateSafePoint();
        String factor = factor();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.MULT_OP) {
                tokenizer.CargarSafePoint();
                String term = term1();
                return term;
            } else {
                return "term => factor\n" + factor;
            }
        } else {
            return "term => factor\n" + factor;
        }
    }

    private String simple_expr1() {
        String simple_expr = simple_expr();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.ADD_OP) {
                String term = term();
                return "simple_expr => simple_expr ADD_OP(" + t.valor + ") term\n" + simple_expr + term;
            } else {
                return "simple_expr => ERROR se esperaba un ADD_OP\n";
            }
        } else {
            return "simple_expr => ERROR no hay mas tokens\n";
        }
    }

    private String factor() {
        tokenizer.CreateSafePoint();
        String factor1 = factor1();
        if (factor1.contains("ERROR")) {
            tokenizer.CargarSafePoint();
            String factor2 = factor2();
            if (factor2.contains("ERROR")) {
                tokenizer.CargarSafePoint();
                String factor3 = factor3();
                if (factor3.contains("ERROR")) {
                    tokenizer.CargarSafePoint();
                    String factor4 = factor4();
                    if (factor4.contains("ERROR")) {
                        tokenizer.CargarSafePoint();
                        String factor5 = factor5();
                        if (factor5.contains("ERROR")) {
                            return factor5;
                        } else {
                            return factor5;
                        }
                    } else {
                        return factor4;
                    }
                } else {
                    return factor3;
                }
            } else {
                return factor2;
            }

        } else {
            return factor1;
        }
    }

    private String term1() {
        String term = term();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.MULT_OP) {
                String factor = factor();
                return "term => term MULT_OP(" + t.valor + ") factor\n" + term + factor;
            } else {
                return "term => ERROR se esperaba un MULT_OP\n";
            }
        } else {
            return "term => ERROR no hay mas tokens\n";
        }
    }

    private String factor1() {
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.valor.matches("\\(")) {
                String bool_expr = bool_expr();
                t = tokenizer.GetNextToken();
                if (t.valor.matches("\\)")) {
                    return "factor => ( bool_expr )\n" + bool_expr;
                } else {
                    return "factor => ERROR se esperaba )\n";
                }
            } else {
                return "factor => ERROR se esperaba (\n";
            }
        } else {
            return "factor => ERROR ya no hay tokens\n";
        }
    }

    private String factor2() {
        String constant = constant();
        return "factor => constant\n" + constant;
    }

    private String factor3() {
        String field = field();
        return "factor => field\n" + field;
    }

    private String factor4() {
        String function = function();
        return "factor => function\n" + function;
    }

    private String factor5() {
        String eval = eval();
        return "factor => eval\n" + eval;
    }

    private String constant() {
        tokenizer.CreateSafePoint();
        if (tokenizer.HasValues()) {
            Token t = tokenizer.GetNextToken();
            if (t.tipo == TIPO.INT_LITTERAL || t.tipo == TIPO.REAL_LITTERAL || t.tipo == TIPO.DATE_LITTERAL
                    || t.tipo == TIPO.STRING_LITTERAL || t.tipo == TIPO.TRUE_LITTERAL || t.tipo == TIPO.FALSE_LITTERAL
                    || t.tipo == TIPO.ID) {

                return "constant => " + t.tipo.name() + "(" + t.valor + ")\n";
            } else {
                tokenizer.CargarSafePoint();
                String qualifier = qualifier();
                return "constant => qualifier\n" + qualifier;
            }
        } else {
            return "constant => ERROR ya no hay tokens";
        }
    }

}
